class AuthenticationResponse {
  final String userId;
  final String nameUser;
  final String token;

  AuthenticationResponse({
    required this.nameUser,
    required this.userId,
    required this.token,
  });
  static AuthenticationResponse fromJson(Map<String, dynamic> json){
    return AuthenticationResponse(
      nameUser: json['user']['name'],
      userId: json['user']['_id'],
      token: json['token'],
    );
  }
}