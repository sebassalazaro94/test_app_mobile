class PostDataResponse{
  List<dynamic> listVariables;

  PostDataResponse({
    required this.listVariables,
  });

  static PostDataResponse fetchVars(List<dynamic> _listVar) {
    List<dynamic> _list = [];
    for(int i=0; i<_listVar.length; i++){
      _list.add(_listVar[i]);
    }
    return PostDataResponse(listVariables: _list);
  }
}