
class Session{
  final String token;
  final String userId;
  final String userName;
  DateTime createdAt;

  Session({
    required this.token,
    required this.userId,
    required this.userName,
    required this.createdAt,
  });

  static Session fromJson(Map<String,dynamic> json){
    return Session(
        token:      json['token'],
        userId:     json['userId'],
        userName:   json['userName'],
        createdAt:  DateTime.parse(json['createdAt']),
    );
  }
  Map<String,dynamic> toJson(){
    return {
      'token'     :token,
      'userId'    :userId,
      'userName'  :userName,
      'createdAt' :createdAt.toString(),

    };
  }

}