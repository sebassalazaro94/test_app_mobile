class ListVariables{
  List<dynamic> listVariables;

  ListVariables({
    required this.listVariables,
  });

  static ListVariables fetchVars(List<dynamic> _listVar) {
    List<dynamic> _list = [];
    for(int i=0; i<_listVar.length; i++){
      _list.add(_listVar[i]);
    }
    return ListVariables(listVariables: _list);
  }
}