class ListPoints{
  List<dynamic> listPoints;

  ListPoints({
    required this.listPoints,
  });

  static ListPoints fetchPoints(List<dynamic> _listPoints) {
    List<dynamic> _list = [];
    for(int i=0; i<_listPoints.length; i++){
      _list.add(_listPoints[i]);
    }
    return ListPoints(listPoints: _list);
  }
}