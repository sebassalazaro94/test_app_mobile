class InfoPoint {
  InfoPoint({
    required this.id,
    required this.tagId,
    required this.name,
    required this.icon,
  });
  final String id;
  final String tagId;
  final String name;
  final String icon;

  factory InfoPoint.fromJson(Map<String, dynamic> json) => InfoPoint(
    id: json["_id"],
    tagId: json["tagId"],
    name: json["name"],
    icon: json["icon"],
  );
  Map<String, dynamic> toJson() => {
    "_id": id,
    "tagId": tagId,
    "name": name,
    "icon": icon,
  };
}
