import 'dart:io';
import 'dart:convert';
import 'package:logger/logger.dart';

class JsonListener {
  Future<String> postData(
      String tagID, int date, String sns, double value) async {
    String data = json.encode({
      tagID: {
        "save": [
          {"t": date, sns: value}
        ]
      }
    });
    Socket.connect('test.lynks.com.co', 3380).then((socket) {
      socket.listen((List<int> event) {
        if (utf8.decode(event) == '\r\nWelcome.\r\n') {
          print(data);
          socket.write(data);
        }
      });
    });

    return '';
  }
}
//{"HOLME-63":{"save":[{"t":1648248300000,"tm00":0}]}}
//{"TENSI-01":{"save":[{"t":1648266515000,"lt00":12.5}]}}