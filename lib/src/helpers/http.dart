import 'package:dio/dio.dart';
import 'package:logger/logger.dart';
import '../helpers/http_response.dart';

//const String noFoundUser ='{"error":{"error":"AUTH_ERROR","reason":"Usuario no encontrado"}}';
//const String passIncorret ='{"error":{"error":"AUTH_ERROR","reason":"Contraseña incorrecta"}}';
Map<String,dynamic> noFoundUser = {'error': {'error': 'AUTH_ERROR', 'reason': 'Usuario no encontrado'}};
Map<String,dynamic> passIncorret = {'error': {'error': 'AUTH_ERROR', 'reason': 'Contraseña incorrecta'}};
const String userInvalid = 'Usuario inválido';
const String passInvalid = 'Constraseña Incorrecta';

class Http {
  Dio? _dio;
  Logger? _logger;
  //bool? _logsEnable;

  Http({
    required Dio dio,
    required Logger logger,
    required bool logsEnable
  }) {
    _dio = dio;
    _logger = logger;
    //_logsEnable = logsEnable;
  }
  Future<HttpResponse<T>> request<T>(
    String path, {
    String method = 'POST',
    Map<String, dynamic>? queryParameters,
    Map<String, dynamic>? data,
    Map<String, String>? headers,
    T Function(dynamic data)? parser,
  }) async {
    try {//statusCode = 20
      final response = await _dio!.request(
        path,
        options: Options(
          method: method,
          headers: headers,
        ),
        queryParameters: queryParameters,
        data: data,
        //'https://app.lynks.com.co/login1',
      );
      //_logger?.i(response.data);
      if(parser!=null){
        //_logger!.i('successData');
        return HttpResponse.success<T>(parser(response.data));
      }else {
        //_logger!.i('noData');
        return HttpResponse.success<T>(response.data);
      }
    } catch (e) {
      //statusCode != 200 - errorCapture

      int statusCode = 0;
      String message = 'unknow error';
      dynamic data;

      if (e is DioError) {
        statusCode = -1;
        message = e.message;
        if (e.response != null) {
          statusCode = e.response!.statusCode!;
          message = e.response!.statusMessage!;
          data = e.response!.data;
        }
      }

      return HttpResponse.fail(
          statusCode: statusCode, message: message, data: data);
    } //catch
  }
}
