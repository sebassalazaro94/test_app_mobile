import 'package:dio/dio.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get_it/get_it.dart';
import 'package:logger/logger.dart';
import 'package:test_login/src/api/account_api.dart';
import 'package:test_login/src/api/authentication_api.dart';
import 'package:test_login/src/api/info_point_api.dart';
import 'package:test_login/src/data/authentication_client.dart';
import 'package:test_login/src/models/authentication_response.dart';
import 'package:test_login/src/models/list_points.dart';

import 'http.dart';

abstract class DependencyInjection {
  static void initialize() {
    final Dio dio = Dio(BaseOptions(baseUrl: 'http://test.lynks.com.co:8090'));
    Logger logger = Logger();
    Http _http = Http(
      dio: dio,
      logger: logger,
      logsEnable: true,
    );

    const secureStorage = FlutterSecureStorage();
    final authenticationAPI = AuthenticationAPI(_http);
    final authenticationClient = AuthenticationClient(secureStorage);
    final accountAPI = AccountAPI(_http, authenticationClient);
    final infoPointAPI = InfoPointAPI(_http, authenticationClient);
    
    GetIt.instance.registerSingleton<AuthenticationAPI>(authenticationAPI);
    GetIt.instance.registerSingleton<AuthenticationClient>(authenticationClient);
    GetIt.instance.registerSingleton<AccountAPI>(accountAPI);
    GetIt.instance.registerSingleton<InfoPointAPI>(infoPointAPI);
  }
}
