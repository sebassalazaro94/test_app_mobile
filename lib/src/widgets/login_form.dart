// ignore_for_file: deprecated_member_use

import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:logger/logger.dart';

import '../api/authentication_api.dart';
import '../data/authentication_client.dart';
import '../widgets/input_text.dart';
import '../utils/dialogs.dart';
import '../utils/responsive.dart';
import '../widgets/user_pass_Error.dart';

class LoginForm extends StatefulWidget {
  const LoginForm({Key? key}) : super(key: key);

  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  final GlobalKey<FormState> _formKey = GlobalKey();
  final VisibilityErrors _visibilityErrors = VisibilityErrors();

  final Logger _logger = Logger();
  bool _isVisible = false;
  String textError = '';
  String _user = '';
  String _pass = '';

  Future<void> _sumit() async {
    final isOK = _formKey.currentState?.validate();
    if (isOK!) {
      ProgressDialog.show(context);
      final authenticationAPI = GetIt.instance<AuthenticationAPI>();
      final authenticationClient = GetIt.instance<AuthenticationClient>();
      final response =
          await authenticationAPI.login(username: _user, password: _pass);
      ProgressDialog.dissmiss(context);
      if (response.data != null) {
        await authenticationClient.saveSession(response.data!);
        //_logger.i(response.data?.nameUser);
        Navigator.pushNamedAndRemoveUntil(context, 'ViewPoint', (route) => false);
      } else {
        setState(() {
          textError = 'usuario o contraseña incorrecta';
          _isVisible = true;
        });
        _logger.e('singin error : ${response.data}');
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    final Responsive responsive = Responsive.of(context);
    return Positioned(
      bottom: 30,
      left: 20,
      right: 20,
      child: Form(
        key: _formKey,
        child: Column(
          children: [
            InputText(
              keyboardType: TextInputType.emailAddress,
              label: 'Usuario',
              onChanged: (text) {
                _user = text!;
                setState(() {
                  _isVisible = false;
                });
              },
              validator: (text) {
                if (text == null) {
                  return "* Requerido";
                } else if (!text.contains('@')) {
                  return 'Usuario Invalido';
                } else {
                  return null;
                }
              },
            ),
            SizedBox(
              height: responsive.dp(1.5),
            ),
            InputText(
              obscureText: true,
              label: 'Contraseña',
              onChanged: (text) {
                _pass = text!;
                setState(() {
                  _isVisible = false;
                });
              },
              validator: (text) {
                if (text == null) {
                  return "* Requerido";
                } else if (text.trim().isEmpty) {
                  return 'Contraseña inválida';
                } else {
                  return null;
                }
              },
            ),
            _visibilityErrors.textUserPassError(_isVisible, textError),
            SizedBox(
              width: double.infinity,
              child: FlatButton(
                child: Text(
                  'Iniciar sesión',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: responsive.dp(1.5),
                  ),
                ),
                color: const Color.fromRGBO(0, 128, 255, 1),
                onPressed: _sumit,
              ),
            ),
            SizedBox(
              height: responsive.dp(15),
            ),
          ],
        ),
      ),
    );
  }
}
