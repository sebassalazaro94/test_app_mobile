import 'package:flutter/material.dart';

import '../models/info_point.dart';
String idPoint = '';
String namePoint = '';
String tagId = '';
class ViewPointsCards{
  List<Widget> listPoints(List<dynamic> list, BuildContext context) {

    final List<Widget> puntos = [];
    for(var opt in list){

      String icon = iconPoint(opt['icon']);
      String element = opt['name'];
      String type = 'mn';

      final widgetPunto = Card(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        margin: const EdgeInsets.all(10),
        elevation: 10.0,
        child: Column(
          children: <Widget>[
            cardOFPoint(context, element, icon, type, opt),
          ],
        ),
      );
      puntos..add(widgetPunto)..add(const Divider(height: 0.5));

    }
    return puntos;
  }
  Widget cardOFPoint(BuildContext context, String element, String icon, String type, Map<String,dynamic> opt){
    return ListTile(
      contentPadding: const EdgeInsets.fromLTRB(15, 10, 15, 10),
      title: Text(element, style: const TextStyle(fontSize: 18)),
      leading: Image(image: AssetImage("assets/images/$icon")),
      trailing: const Icon(Icons.arrow_forward_ios),
      onTap: (){
          if(type == 'mn' ){
            final data = InfoPoint.fromJson(opt);
            idPoint = data.id;
            namePoint = data.name;
            tagId = data.tagId;
            Navigator.pushNamedAndRemoveUntil(context, 'ViewVars', (route) => false);
          }
        },
    );
  }
  String iconPoint(String icon){
    if(icon == ''){
      return '_point_.png';
    }else{
      return icon;
    }
  }
}