// ignore_for_file: unnecessary_null_comparison

import 'package:flutter/material.dart';

class Circle extends StatelessWidget{
  //declaration of widget circle variables
  final double size;


  const Circle({
    Key? key,
    required this.size,
  })
        : assert(size != null && size > 0),
          super(key: key);

  @override
  Widget build(BuildContext context){
    return Container(
      width: size,
      height: size,
      decoration: const BoxDecoration(
        color: Colors.black12,
        shape: BoxShape.circle,
      ),
    );
  }
}