import 'package:flutter/material.dart';


class InputText extends StatelessWidget{
  final String label;
  final TextInputType keyboardType;
  final bool obscureText;
  final double fontSize;
  final void Function(String? val)? onChanged;
  final String? Function(String? val)? validator;

  const InputText({
    Key? key,
    this.label='',
    this.keyboardType = TextInputType.text,
    this.obscureText = false,
    this.fontSize = 15,
    this.onChanged,
    this.validator,
   }) :super(key: key);

  @override
  Widget build(BuildContext context) {

    return TextFormField(
      validator: validator,
      keyboardType: keyboardType,
      obscureText: obscureText,
      onChanged: onChanged,
      style: TextStyle(fontSize: fontSize),
      decoration: InputDecoration(
        labelText: label,
        labelStyle: const TextStyle(
          fontWeight: FontWeight.w500,
          color: Colors.black45,
        ),

      ),
    );
  }
}