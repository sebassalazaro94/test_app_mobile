// ignore_for_file: unnecessary_null_comparison

import 'package:flutter/material.dart';

class IconContainer extends StatelessWidget{
  final double size;
  const IconContainer({Key? key, required this.size})
      : assert(size != null && size > 0),
        super(key: key);


  @override
  Widget build(BuildContext context) {
    return  Container(
      padding: EdgeInsets.all(size*0.15),
      width: size,
      height: size,
      decoration: BoxDecoration(
        color: const Color.fromRGBO(84, 183, 250, 1.0),
        borderRadius: BorderRadius.circular(size*0.2),
        boxShadow: const [
          BoxShadow(
            color: Colors.grey,
            blurRadius: 10,
            spreadRadius: 1.0,
          ),

        ],
      ),

      child: const Center(
          child: Image(image: AssetImage('assets/images/logoBlanco.png')
        ),
      ),
    );
  }
}