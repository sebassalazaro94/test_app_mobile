import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:logger/logger.dart';
import 'package:test_login/src/api/account_api.dart';

import '../widgets/view_points_cards.dart';

class InfoPointForm extends StatefulWidget{

  const InfoPointForm({Key? key}) : super(key: key);

  @override
  _InfoPointForm createState() => _InfoPointForm();
}

class _InfoPointForm extends State<InfoPointForm>{

  //List<dynamic> _listPoints = [];
  final ViewPointsCards _viewPointsCards = ViewPointsCards();
  final _accountAPI = GetIt.instance<AccountAPI>();
  final Logger _logger = Logger();

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance?.addPostFrameCallback((_) {
      _loadListPoints;
    });
  }
  Future<List<dynamic>> _loadListPoints() async{
    final response = await _accountAPI.getInfoPoints();
    if (response.data != null) {
      //_logger.i(_listPoints.runtimeType);
      //_logger.i(response.data?.listPoints);
      return response.data?.listPoints as List;
    }else{
      return [];
    }
  }

  @override
  Widget build(BuildContext context) {

    //_logger.i('$_listPoints');
    return FutureBuilder(
        future: _loadListPoints(),
        builder: (context, AsyncSnapshot<List<dynamic>> snapshot){
          if(snapshot.hasData){
            return ListView(
              children: _viewPointsCards.listPoints(snapshot.requireData, context),
            );
          }
          return const Center(child: CircularProgressIndicator(),);
        },
    );

  }

}