import 'package:flutter/material.dart';

String sns = '';

class ViewVarsCards {
  List<Widget> listVars(List<dynamic> list, BuildContext context) {
    final List<Widget> puntos = [];
    for (var opt in list) {
      String _type = opt['type'];
      String element = opt['label'];
      if (_type == 's') {
        String _sns = opt['options']['sensor'];
        if (_sns == 'lt00') {
          final widgetPunto = Card(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            margin: const EdgeInsets.all(10),
            elevation: 10.0,
            child: Column(
              children: <Widget>[
                cardOFPoint(element, _type, context, opt),
              ],
            ),
          );
          puntos
            ..add(widgetPunto)
            ..add(const Divider(height: 0.5));
        }
      }
    }
    return puntos;
  }

  Widget cardOFPoint(String element, String type, BuildContext context,
      Map<String, dynamic> opt) {
    sns = opt['options']['sensor'];
    double value = 12.5;
    return ListTile(
      contentPadding: const EdgeInsets.fromLTRB(15, 10, 15, 10),
      title: Text(element, style: const TextStyle(fontSize: 18)),
      trailing: const Icon(Icons.arrow_forward_ios),
      onTap: () {
        Navigator.pushNamedAndRemoveUntil(
            context, 'SendData', (route) => false);
      },
    );
  }
}
