import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:logger/logger.dart';
import 'package:test_login/src/api/info_point_api.dart';
import 'package:test_login/src/widgets/view_vars_cards.dart';

import '../widgets/view_points_cards.dart';


class InfoVarsForm extends StatefulWidget{

  const InfoVarsForm({Key? key}) : super(key: key);
  @override
  _InfoVarsForm createState() => _InfoVarsForm();
}

class _InfoVarsForm extends State<InfoVarsForm>{
  //List<dynamic> _listPoints = [];
  final ViewVarsCards _viewVarsCards = ViewVarsCards();
  final _infoPointsApi = GetIt.instance<InfoPointAPI>();
  final Logger _logger = Logger();
  final String _idPoint = idPoint;
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance?.addPostFrameCallback((_) {
      _loadListVars(_idPoint);
    });
  }
  Future<List<dynamic>> _loadListVars(String idPoint) async{
    final response = await _infoPointsApi.getInfoVar(idPoint);
    //_logger.i(response.data?.listVariables);
    if (response.data != null) {
      //_logger.i(_listPoints.runtimeType);
      //_logger.i(response.data?.listPoints);
      return response.data?.listVariables as List;
    }else{
      return [];
    }
  }

  @override
  Widget build(BuildContext context) {

    //_logger.i('$_listPoints');
    return FutureBuilder(
      future: _loadListVars(_idPoint),
      builder: (context, AsyncSnapshot<List<dynamic>> snapshot){
        if(snapshot.hasData){
          return ListView(
            children: _viewVarsCards.listVars(snapshot.requireData, context),
          );
        }
        return const Center(child: CircularProgressIndicator());
      },
    );

  }


}