// ignore_for_file: file_names

import 'package:flutter/material.dart';

class VisibilityErrors {
  Widget textUserPassError(bool isVisible, String text){
    return Visibility(
      visible: isVisible,
      maintainSize: true,
      maintainAnimation: true,
      maintainState: true,
      child: Container(
        alignment: Alignment.center,
        padding: const EdgeInsets.all(10),
        child: Text(
          text,
          style: const TextStyle(
            color: Colors.redAccent,
            fontSize: 20,
          ),
        ),
      ),
    );
  }
}
