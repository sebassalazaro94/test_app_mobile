// ignore_for_file: prefer_final_fields, deprecated_member_use

import 'package:date_time_picker/date_time_picker.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:test_login/src/widgets/view_points_cards.dart';
import 'package:test_login/src/widgets/view_vars_cards.dart';

import '../helpers/json_listener.dart';
import '../utils/responsive.dart';
import 'input_text.dart';

int kDate = 3600000;
const snackBar = SnackBar(
  content: Text('Datos enviados'),
);

class DataPostVarForm extends StatefulWidget {
  const DataPostVarForm({Key? key}) : super(key: key);

  @override
  _DataPostVarForm createState() => _DataPostVarForm();
}

class _DataPostVarForm extends State<DataPostVarForm> {
  final GlobalKey<FormState> _oFormKey = GlobalKey<FormState>();
  late TextEditingController _controller1;
  String dateChanged = '';
  String dateToValidate = '';
  String dateSaved = '';
  double numberDouble = 0.0;

  @override
  void initState() {
    super.initState();
    Intl.defaultLocale = 'es_ES';
    //_initialValue = DateTime.now().toString();
    _controller1 = TextEditingController(text: DateTime.now().toString());

    _getValue();
  }

  /// This implementation is just to simulate a load data behavior
  /// from a data base sqlite or from a API
  Future<void> _getValue() async {
    await Future.delayed(const Duration(seconds: 3), () {
      setState(() {
        _controller1.text = DateTime.now().toString();
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    final Responsive responsive = Responsive.of(context);
    return SingleChildScrollView(
      padding: EdgeInsets.all(responsive.dp(5.0)),
      child: Form(
        key: _oFormKey,
        child: Column(
          children: <Widget>[
            SizedBox(height: responsive.hp(5)),
            DateTimePicker(
              type: DateTimePickerType.dateTimeSeparate,
              dateMask: 'd MMMM, yyyy',
              controller: _controller1,
              //initialValue: _initialValue,
              firstDate: DateTime(2022),
              lastDate: DateTime(2050),
              //icon: Icon(Icons.event),
              dateLabelText: 'Fecha',
              timeLabelText: 'Hora',
              use24HourFormat: true,
              locale: const Locale('es', 'ES'),
              onChanged: (val) => setState(() => dateChanged = val),
              validator: (val) {
                setState(() => dateToValidate = val ?? '');
                return null;
              },
              onSaved: (val) => setState(() => dateSaved = val ?? ''),
            ),
            SizedBox(height: responsive.hp(5)),
            InputText(
              keyboardType: TextInputType.number,
              label: 'Litros',
              validator: (text) {
                if (text == null) {
                  return "* Requerido";
                } else if (!text.contains('.')) {
                  return 'Número Inválido';
                } else {
                  return null;
                }
              },
              onChanged: (text) {
                numberDouble = double.parse(text!);
              },
            ),
            SizedBox(height: responsive.hp(5)),
            SizedBox(
              height: responsive.hp(5),
              width:  responsive.wp(60),
              child: FlatButton(
                color: Colors.blue,
                textColor: Colors.white,
                onPressed: () {
                  final loForm = _oFormKey.currentState;

                  if (loForm?.validate() == true) {
                    loForm?.save();
                    final _dateOffset = DateTime.parse(dateSaved).timeZoneOffset.inHours;
                    final _dateGMT = DateTime.parse(dateSaved);
                    final _dateTs = _dateGMT.microsecondsSinceEpoch;
                    int date = ((_dateTs/1000)+(_dateOffset*kDate)).round();
                    print(_dateOffset*kDate);
                    JsonListener().postData(tagId, date, sns, numberDouble);
                  }


                },
                child: const Text('Subir', style: TextStyle(fontSize: 20),),
              ),
            ),
            SizedBox(height: responsive.hp(1.5)),
            //SelectableText(dateSaved),
          ],
        ),
      ),
    );
  }
}
/*
final fToast = FToast();
                  fToast.init(context);
                  Fluttertoast.showToast(
                      msg: "Datos enviados",
                      toastLength: Toast.LENGTH_SHORT,
                      gravity: ToastGravity.CENTER,
                      timeInSecForIosWeb: 1,
                      backgroundColor: Colors.red,
                      textColor: Colors.white,
                      fontSize: 16.0
                  );
 */
