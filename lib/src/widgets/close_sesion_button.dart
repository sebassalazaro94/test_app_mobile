// ignore_for_file: deprecated_member_use

import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import '../data/authentication_client.dart';
import '../utils/responsive.dart';

class CloseSesionButton extends StatelessWidget{
  CloseSesionButton({Key? key}) : super(key: key);

  final AuthenticationClient _authenticationClient = GetIt.instance<AuthenticationClient>();

  @override
  Widget build(BuildContext context) {
    final Responsive responsive = Responsive.of(context);
    return SizedBox(
      width: responsive.wp(20),
      child: RaisedButton(
        child: const Text(
           'Cerrar\nsesión',
           style: TextStyle(color: Colors.blue),
         ),
        onPressed: () async{
          await _authenticationClient.signOut();
          Navigator.restorablePushNamedAndRemoveUntil(context, 'Login', (route) => false);
        }
      )
    );
  }
}