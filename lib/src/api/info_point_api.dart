import 'package:logger/logger.dart';
import 'package:test_login/src/helpers/http_response.dart';
import 'package:test_login/src/models/list_variables.dart';

import '../data/authentication_client.dart';
import '../helpers/http.dart';

class InfoPointAPI{
  final Logger _logger = Logger();
  final Http _http;
  final AuthenticationClient _authenticationClient;

  InfoPointAPI(this._http, this._authenticationClient);

  Future<HttpResponse<ListVariables>> getInfoVar(String? _idPoint) async{
    final token = await _authenticationClient.accesToken;
    return _http.request<ListVariables>(
        'http://test.lynks.com.co:8090/api/points/$_idPoint/variables',
        method: 'GET',
        headers: {
          'Content-type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'JWT $token'
        },parser: (data){
          final List<dynamic> _listVar = data;
          return ListVariables.fetchVars(_listVar);
        }
    );
  }

}