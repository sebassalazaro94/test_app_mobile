import '../helpers/http_response.dart';
import '../helpers/http.dart';
import '../models/authentication_response.dart';

class AuthenticationAPI {
  final Http _http;

  AuthenticationAPI(this._http);

  Future<HttpResponse<AuthenticationResponse>> login({
    required String username,
    required String password,
  }) {
    return _http.request<AuthenticationResponse>(
        'http://test.lynks.com.co:8090/login1',
        method: "POST",
        data: {'email': username, 'password': password},
        parser: (data) {
          return AuthenticationResponse.fromJson(data);
        }
    );
  }
}
