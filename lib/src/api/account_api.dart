import 'dart:async';

import 'package:logger/logger.dart';
import 'package:test_login/src/helpers/http.dart';
import 'package:test_login/src/helpers/http_response.dart';
import 'package:test_login/src/models/list_points.dart';

import '../data/authentication_client.dart';

class AccountAPI{
  final Logger _logger = Logger();
  final Http _http;
  final AuthenticationClient _authenticationClient;

  AccountAPI(this._http, this._authenticationClient);

  Future<HttpResponse<ListPoints>> getInfoPoints() async{
    final token = await _authenticationClient.accesToken;
    return _http.request<ListPoints>(
      'http://test.lynks.com.co:8090/api/points',
      method: 'GET',
      headers: {
        'Content-type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'JWT $token'
      },parser: (data){
        final List<dynamic> _listPoints = data;
        return ListPoints.fetchPoints(_listPoints);
      }
    );
  }
}