import 'dart:convert';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:test_login/src/models/authentication_response.dart';
import 'package:test_login/src/models/session.dart';

class AuthenticationClient{
  final FlutterSecureStorage _secureStorage;

  AuthenticationClient(this._secureStorage);
  Future<String?> get accesToken async {
   final data = await _secureStorage.read(key: 'SESSION');
   if(data != null){
     final session = Session.fromJson(jsonDecode(data));
     return session.token;
   }
   return null;
  }
  Future<void> saveSession(AuthenticationResponse authenticationResponse) async {
    final Session session = Session(
        token: authenticationResponse.token,
        userId: authenticationResponse.userId,
        userName: authenticationResponse.nameUser,
        createdAt: DateTime.now(),
    );
    final data = jsonEncode(session.toJson());
   await _secureStorage.write(key: 'SESSION', value: data);
  }
  Future<void> signOut() async {
    _secureStorage.deleteAll();
  }

}