import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:test_login/src/pages/Splash.dart';
import 'package:test_login/src/pages/data_post_var.dart';
import 'package:test_login/src/pages/loginScreen.dart';
import 'package:test_login/src/pages/viewVars.dart';
import 'package:test_login/src/pages/viewsPoints.dart';

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Login Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        fontFamily: 'sans-serif-condensed',
      ),
      home: const SplashPage(),
      routes: <String, WidgetBuilder>{
        'Login': (BuildContext context) => const LoginScreen(),
        'ViewPoint': (BuildContext context) => const ViewsPoints(),
        'ViewVars': (BuildContext context) => const ViewsVars(),
        'SendData': (BuildContext context) => const DataPostVar(),
      },
      localizationsDelegates: const [
        GlobalWidgetsLocalizations.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: const [Locale('es', 'ES')],
    );
  }
}
