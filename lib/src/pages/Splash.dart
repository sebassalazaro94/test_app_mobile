import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:test_login/src/data/authentication_client.dart';

class SplashPage extends StatefulWidget{
  const SplashPage({Key? key}) : super(key: key);

  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage>{
  final _authenticationClient = GetIt.instance<AuthenticationClient>();
  @override
  void initState(){
    super.initState();
    WidgetsBinding.instance?.addPostFrameCallback((_) {
      _checkLogin();
    });
  }
  Future<void> _checkLogin() async{
    final token = await _authenticationClient.accesToken;
    if(token!=null){
      Navigator.pushReplacementNamed(context, 'ViewPoint');
      return;
    }
    Navigator.pushReplacementNamed(context, 'Login');
  }
  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: Center(
        child: CircularProgressIndicator(),
      ),
    );
  }
}