import 'package:flutter/material.dart';
import 'package:test_login/src/widgets/icon_container.dart';
import '../utils/responsive.dart';
import '../widgets/close_sesion_button.dart';
import '../widgets/data_post_var_form.dart';

class DataPostVar extends StatefulWidget {
  const DataPostVar({Key? key}) : super(key: key);

  @override
  _DataPostVar createState() => _DataPostVar();
}

class _DataPostVar extends State<DataPostVar> {
  @override
  Widget build(BuildContext context) {
    final Responsive responsive = Responsive.of(context);
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'Subir datos',
          style: TextStyle(fontSize: 20),
        ),
        actions: [
          CloseSesionButton(),
        ],
        leading: IconButton(
          icon: const Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => Navigator.pushNamed(context, 'ViewVars'),
        ),
      ),
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: SingleChildScrollView(
          child: Container(
            width: double.infinity,
            height: responsive.height,
            color: Colors.white,
            child: Stack(
              children: const [
                DataPostVarForm(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
