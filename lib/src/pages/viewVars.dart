// ignore_for_file: file_names

import 'package:flutter/material.dart';
import 'package:logger/logger.dart';
import 'package:test_login/src/widgets/close_sesion_button.dart';
import 'package:test_login/src/widgets/info_vars_form.dart';
import 'package:test_login/src/widgets/view_points_cards.dart';

class ViewsVars extends StatefulWidget {
  const ViewsVars({Key? key}) : super(key: key);
  @override
  _ViewsVars createState() => _ViewsVars();
}

class _ViewsVars extends State<ViewsVars> {

  final Logger _logger = Logger();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Variables \n$namePoint',
          style: const TextStyle(fontSize: 16),
        ),
        actions: [
          CloseSesionButton(),
        ],
        leading: IconButton(
          icon: const Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => Navigator.pushNamed(context, 'ViewPoint'),
        ),
      ),
      body: const InfoVarsForm(),
    );
  }
}