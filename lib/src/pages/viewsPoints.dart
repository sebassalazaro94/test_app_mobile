// ignore_for_file: deprecated_member_use
import 'package:flutter/material.dart';
import 'package:logger/logger.dart';
import 'package:test_login/src/widgets/close_sesion_button.dart';
import 'package:test_login/src/widgets/info_point_form.dart';

class ViewsPoints extends StatefulWidget {
  const ViewsPoints({Key? key}) : super(key: key);

  @override
  _ViewsPoints createState() => _ViewsPoints();
}

class _ViewsPoints extends State<ViewsPoints> {

  final Logger _logger = Logger();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Puntos'),
          actions: [
            CloseSesionButton(),
          ],
        ),
        body: const InfoPointForm(),
      ),
    );
  }
}