// ignore_for_file: file_names

import 'package:flutter/material.dart';

import 'package:test_login/src/widgets/icon_container.dart';
import '../utils/responsive.dart';
import '../widgets/login_form.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  @override
  Widget build(BuildContext context) {
    final Responsive responsive = Responsive.of(context);
    return Scaffold(
      body: GestureDetector(
        onTap: (){
          FocusScope.of(context).unfocus();
        },
        child: SingleChildScrollView(
          child: Container(
            width: double.infinity,
            height: responsive.height,
            color: Colors.white,
            child: Stack(
              children: [
                Positioned(
                  top: responsive.hp(20),
                  left: responsive.wp(42),
                  child: Column(children: [
                    IconContainer(
                      size: responsive.wp(16),
                    ),
                    SizedBox(
                      height: responsive.dp(1.6),
                    ),
                    Text(
                      'Lynks',
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: responsive.dp(1.5)),
                    ),
                  ]),
                ),
                const LoginForm(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
