import 'package:flutter/material.dart';
import 'package:test_login/src/app.dart';
import 'package:test_login/src/helpers/dependency_injection.dart';

void main() {
  DependencyInjection.initialize();
  runApp(const MyApp());
}
